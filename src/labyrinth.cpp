#include "ros/ros.h"
#include "ros/time.h"

#include "turtlesim/Pose.h"
#include "turtlesim/Color.h"
#include "geometry_msgs/Twist.h"
#include "math.h"

#include <tf2_ros/buffer.h>
#include "tf/transform_broadcaster.h"
#include <turtlesim/Spawn.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <turtlesim/Kill.h>



turtlesim::Pose current_pose; 

geometry_msgs::Twist speed; 
double current_distance, current_angle, relative_angle; 


/* function prototype*/
//void move(double speed, double distance, bool isForward);
//void rotate(double angular_speed, double speed, bool clockwise);
//double setDesiredOrientation(double desired_angle_radians);
//void moveGoal(turtlesim::Pose goal_pose, double distance_tolerance);


const double PI = 3.14159265359;

  
  ros::Subscriber sub_pose; 
  ros::Subscriber sub_pose_turtle2; 
  
  ros::Publisher vel_pub ;


  void stop(){

  double t1 = 0; 
  double t = ros::Time::now().toSec();
  ROS_INFO("[turtle1] stop");
    while(ros::ok && ((t1-t) < 1)) {


     
     t1 = ros::Time::now().toSec();
      speed.linear.x = 0; 
      vel_pub.publish(speed);

    }
  }

  void moveForward(double vitesse, double distance){
  
  double start_time = ros::Time::now().toSec(); 
  current_distance = 0; 
  ROS_INFO("[turtle1] move");
    while(ros::ok && current_distance < distance ){
     
      double current_time = ros::Time::now().toSec(); 
      current_distance = vitesse * (current_time - start_time);
      speed.linear.x = vitesse; 
    
         vel_pub.publish(speed);
    }

      
      ROS_INFO("current distance %f", current_distance);
    /*ROS_INFO("[turtle1] j'arrête");
    speed.linear.x = 0; 
    vel_pub.publish(speed);*/
      //ros::Duration(0.5).sleep();

}

  void turn(double angular_speed, double angle){

    double t0 = ros::Time::now().toSec(); 
    current_angle = 0; 
    relative_angle = angle*PI /180;
    ROS_INFO("[turtle1] turn"); 
    while(ros::ok && current_angle < relative_angle){
      
      speed.angular.z = angular_speed; 
      double t1 = ros::Time::now().toSec();
      current_angle = angular_speed * (t1 - t0); 
      vel_pub.publish(speed); 

    }

      double current_angle_in_degree = (current_angle * 180 )/PI;
      ROS_INFO("current_angle: %f", current_angle_in_degree);


      /*ROS_INFO("[turtle1] j'arrête");
      speed.linear.x = 0; 
      vel_pub.publish(speed);*/

  }

void poseCallback(const turtlesim::PoseConstPtr& msg){
  //linear position
   current_pose.x = msg-> x;
   current_pose.y = msg-> y; 
   current_pose.theta = msg -> theta; 
  
   ROS_INFO("x: %f , y: %f", current_pose.x, current_pose.y);


}


int main(int argc, char **argv){

  ROS_INFO("start simulation");
  
  ros::init(argc, argv, "labyrinth");
  ros:: NodeHandle node; 

  ros::Rate loop_rate(50);

  sub_pose = node.subscribe("/turtle1/pose", 1, poseCallback);

  vel_pub = node.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 1);
  
 int count = 0; 

  while(ros::ok()){

  if(count < 6){
    moveForward(4, 2); 
    stop();
    turn(2, 90);
    moveForward(4, 2); 
    stop();

    count = count + 1; 
  }


    

  ros::spinOnce();

  loop_rate.sleep();  
  }
                                                                       
}

    //ros::Subscriber sub = n.subscribe(turtle_name+"/pose", 10, &poseCallback);

  
